package de.d4v3z02.datboi;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Controller {

	private SynonymManager synonymManager;
	private FileHandler fileHandler;
	
	
	public Controller(){
		synonymManager = new SimpleSynonymManager();
	}
	
	public void addWord(String idWord, String savedWord){
		synonymManager.saveWord(idWord, savedWord);
	}
	
	public void addFileToManager(String filename){
		try {
			synonymManager.addSynonymList(fileHandler.readFile(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeListToFile(String word, String filename){
		try {
			fileHandler.writeFile(synonymManager.get(word), filename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args){
		Controller controller = new Controller();
		controller.addWord("Haus", "Megahaus");
		
	}
	
	
}
