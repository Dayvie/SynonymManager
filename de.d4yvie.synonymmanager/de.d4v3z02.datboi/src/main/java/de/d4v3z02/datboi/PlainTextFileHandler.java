package de.d4v3z02.datboi;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.stream.Collectors;

public class PlainTextFileHandler implements FileHandler {

	/**
	 * Schreibt jedes Wort des �bergebenen HashSets in eine neue Zeile in die angegebene Datei
	 * �berschreibt alte Dateien indem es sie l�scht
	 * @param stringList java.util.List Liste mit W�rtern
	 * @param filename ganzer Dateiname der geschriebenen Datei (mit Endung)
	 * @throws FileNotFoundException 
	 */
	public void writeFile(HashSet<String> stringList, String filename) throws FileNotFoundException{	 
		  try (PrintWriter writer = new PrintWriter(filename);){
			 stringList.stream().forEach(word ->
					writer.println(word));
		}
	}

	@Override
	public HashSet<String> readFile(String filename) throws FileNotFoundException, IOException {
		try(BufferedReader reader = new BufferedReader(new FileReader(filename));){
			return new HashSet<String>(reader.lines().collect(Collectors.toList()));
		}
	}
}