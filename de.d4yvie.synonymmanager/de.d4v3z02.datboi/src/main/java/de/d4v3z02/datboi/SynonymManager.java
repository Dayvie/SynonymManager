package de.d4v3z02.datboi;

import java.util.HashSet;

public interface SynonymManager {
	HashSet<String> get(String word);
	void saveWord(String idWord, String savedWord);
	boolean isWordSaved(String word);
	void addSynonymList(HashSet<String> synonymList);
}
