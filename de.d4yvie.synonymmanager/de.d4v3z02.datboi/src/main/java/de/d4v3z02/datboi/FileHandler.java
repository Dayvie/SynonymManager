package de.d4v3z02.datboi;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;

public interface FileHandler {
	public void writeFile(HashSet<String> arr, String filename)throws FileNotFoundException;
	public HashSet<String>readFile(String filename)throws FileNotFoundException, IOException;
}
