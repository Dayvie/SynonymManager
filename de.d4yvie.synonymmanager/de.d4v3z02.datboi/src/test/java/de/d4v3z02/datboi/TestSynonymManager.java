package de.d4v3z02.datboi;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestSynonymManager {

	SynonymManager synonymManager;
	
	@Before
	public void setUp(){
		synonymManager = new SimpleSynonymManager();
		synonymManager.saveWord("Haus", "MegaHaus");
	}
	
	@Test
	public void testSaveWordPositive() {
		Assert.assertTrue(synonymManager.isWordSaved("Haus"));
		Assert.assertTrue(synonymManager.isWordSaved("MegaHaus"));
	}
	
	@Test
	public void testSaveWordNegative() {
		Assert.assertFalse(synonymManager.isWordSaved("Hi"));
	}

	@Test
	public void getWord(){
		HashSet<String> synonymList = new HashSet<>();
		synonymList.add("Haus");
		synonymList.add("MegaHaus");
		Assert.assertEquals(synonymList, synonymManager.get("Haus"));
	}
	
}
