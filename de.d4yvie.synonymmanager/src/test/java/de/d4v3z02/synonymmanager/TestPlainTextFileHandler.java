package de.d4v3z02.synonymmanager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.d4v3z02.synonymmanager.FileHandler;
import de.d4v3z02.synonymmanager.PlainTextFileHandler;

public class TestPlainTextFileHandler {

	FileHandler fileHandler;
	HashSet<String> testArray;
	
	@Before
	public void setUp(){
		fileHandler = new PlainTextFileHandler();
		testArray = new HashSet<>(Arrays.asList("Hi", "Whaddup"));
	}
	
	@Test
	public void testWriteAndRead() throws FileNotFoundException, IOException {
		fileHandler.writeFile(testArray, "datboi.txt");
		HashSet<String> words = fileHandler.readFile("datboi.txt");
		Assert.assertEquals(new HashSet<String>(Arrays.asList("Hi", "Whaddup")), words);
	}
}
