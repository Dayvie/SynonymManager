package de.d4v3z02.synonymmanager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @author dayvie
 *
 */
public class SimpleSynonymManager implements SynonymManager {

	private List<HashSet> synonymLists;
	
	/**
	 * 
	 */
	public SimpleSynonymManager() {
		synonymLists = new ArrayList<>();
	}
	
	/* 
	 * Gibt eine Liste mit W�rtern zurueck welche die selbe Bedeutung wie das Parameterwort haben
	 * @param synonymousWord das Wort f�r das Synonyme gesucht werden
	 */
	public HashSet<String> get(String synonymousWord) {
		if(isWordSaved(synonymousWord)){
			for(HashSet<String> synonymList: synonymLists){
				if(synonymList.contains(synonymousWord)){
					return synonymList;
				}
			}
		}
		throw new RuntimeException(";D");
	}
	
	
	/*
	 * Speichert ein Wort in synonymLists. Die richtige Synonymliste wird mit dem Parameter ID erkannt, welcher
	 * einem Wort in der Liste entspricht.
	 */
	public void saveWord(String idWord, String savedWord){
		if(isWordSaved(idWord)){
			addSynonym(idWord, savedWord);
		}else{
			addNewList(idWord);
			saveWord(idWord, savedWord);
		}
	}
	
	/**
	 * Erschafft eine neue Synonymenliste
	 * @param idWord Das in der neuen Liste enthaltene Wort
	 */
	public void addNewList(String idWord){
		HashSet<String> newList = new HashSet<>();
		newList.add(idWord);
		synonymLists.add(newList);
	}
	
	/**
	 * 
	 * @param idWord
	 * @param savedWord
	 */
	public void addSynonym(String idWord, String savedWord){
		for(HashSet<String> synonymList: synonymLists){
			if(synonymList.contains(idWord)){
				synonymList.add(savedWord);
			}
		}
	}
	
	/**
	 * @param word Das gesuchte Wort
	 * @return boolean primitive ob das Wort vorhanden ist oder nicht
	 * Gibts zur�ck ob ein Wort in einer der Synonymlisten ist
	 */
	public boolean isWordSaved(String word){
		return synonymLists.stream().anyMatch(synonymList -> synonymList.contains(word));
	}

	@Override
	public void addSynonymList(HashSet<String> synonymList) {
		synonymLists.add(synonymList);
	}
}
